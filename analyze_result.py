#!/usr/bin/env python3

import argparse
import os
from os.path import isfile, join, getsize

def main():
    parser = argparse.ArgumentParser(description='Process data files.')
    parser.add_argument('data_dirs', metavar='DIR', nargs='+', help='directories to analyze')

    args = parser.parse_args()
    print(args.data_dirs)

    for dir in args.data_dirs:
        analyze(dir)

def analyze(dir):
    print("### analyze: {}".format(dir))
    last_lines = dict()
    onlyfiles = [f for f in os.listdir(dir) if isfile(join(dir, f))]
    
    for file in onlyfiles:
        if file == "multi_file_writer":
            continue
        file = join(dir, file)
        if getsize(file) == 0:
            last_lines[file] = 0
            continue
        
        with open(file, 'r') as fd:
            #fd.seek(-64, 2)
            last = fd.readlines()[-1].strip()
            #print("{}: {}".format(file, last))
            last_lines[file] = int(last)
    
    if len(last_lines) == 0:
        print("    empty directory")
        return
    
    maximum = max(last_lines.values())
    print("maximum: {}".format(maximum))
    for file, last in last_lines.items():
        print("    {}: {}".format(file, maximum - last))
        
if __name__ == "__main__":
    main()
