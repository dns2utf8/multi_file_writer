# multi file writer

Writes to multiple files at once simulating a database.

* All options from CLI or environment
* Used to reproduce [a bug with XFS and fsfreeze](https://bugzilla.kernel.org/show_bug.cgi?id=205833)
* No unsafe code

```bash
multi_file_writer 0.4.4
A test utility for simulating a database load on disks

USAGE:
    multi_file_writer [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --clean-target <clean-target>        Clean the output directory before the run [env: MFW_CLEAN_TARGET=]
    -d, --delay <delay>                      Set how many milli seconds to wait between each synchronized append [env:
                                             MFW_DELAY=]  [default: 0.1]
    -m, --mode <mode>                        Choose the mode to run the test in: * synced: checked progress * clock:
                                             fastest progress (accurate to +/- 1 number) * clock-catch-up: like clock
                                             allows to write multiple lines at once if needed [env: MFW_MODE=]
                                             [default: synced]
    -n, --n-files <n-files>                  How many files should we create in parallel [env: MFW_N_FILES=]  [default:
                                             4]
    -o, --output-dir <output-dir>            Output directory [env: MFW_OUTPUT_DIR=]  [default: ./generated]
    -r, --rows <rows>                        How many lines should we create inside each file [env: MFW_ROWS=]
                                             [default: 1_000_000]
    -f, --run-forever <run-forever>          Run forever with random pauses between runs [env: MFW_RUN_FOREVER=]
        --inter-run-delay <seconds-range>    When running forever, how long should the pause between runs be. This can
                                             be a range: <min..max> seconds or just <n> seconds [env:
                                             MFW_INTER_RUN_DELAY=]  [default: 60..120]
```


## Building from source

Clone this repo and make sure to have [rustup](https://rustup.rs/) installed then run this:

```bash
cargo run --release -- [optional arguments]
```


## Using docker container

Running interactive:

```bash
docker run --user $(id -u) --rm -it -v $(pwd)/generated:/generated registry.gitlab.com/dns2utf8/multi_file_writer:latest
```

Running in background:

```bash
docker run --user $(id -u) --name multi_file_writer --detach -v $(pwd)/generated:/generated -e MFW_RUN_FOREVER=true registry.gitlab.com/dns2utf8/multi_file_writer:latest
```

## Testing the setup with kubernetes

First, make sure you don't need the `snapshot-test` namespace on your kubernetes cluster.

Second, verify that kubectl is connected to the correct cluster.

Third, be sure velero and restic are setup.

* `make deploy_k8s`
* `make get_pods`
* `make backup`

### Expected result

When run without arguments multi_file_writer will create 4 files inside the `generated` folder.
Each file is 6.9MB in size and contains the numbers `0` to `999999` separated by `\n`.


## Cleanup

`make cleanup_k8s`


## Getting logs

You have to replace the id with the specific id of the spawned pod.

```
kubectl -n snapshot-test logs multi-file-writer-[press tab here] multi-file-writer 
```

## Troubleshooting

Check that your versions match for example like this:

```bash
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.6", GitCommit:"7015f71e75f670eb9e7ebd4b5749639d42e20079", GitTreeState:"clean", BuildDate:"2019-11-13T11:20:18Z", GoVersion:"go1.12.12", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.3", GitCommit:"2d3c76f9091b6bec110a5e63777c332469e0cba2", GitTreeState:"clean", BuildDate:"2019-08-19T11:05:50Z", GoVersion:"go1.12.9", Compiler:"gc", Platform:"linux/amd64"}
```

### Unfreeze storage by hand

When velero 1.2.0 crashes the PV is stuck because `fsfreeze --unfreeze` was never executed.

One way to fix this is to login the machine (get them with `kubectl get nodes`) and search for the volume and run the command by hand:
```bash
mount | grep pvc-e9105772-9717-4f3b-8538-026fff24faee
# /dev/mapper/mpathgc on /var/lib/kubelet/pods/742605b0-e68c-4260-84e0-713dea05e291/volumes/kubernetes.io~csi/pvc-c6d31065-d48f-4250-a79d-773bf81e726b/mount type xfs (rw,relatime,attr2,inode64,noquota)
sudo fsfreeze --unfreeze /var/lib/kubelet/pods/742605b0-e68c-4260-84e0-713dea05e291/volumes/kubernetes.io~csi/pvc-c6d31065-d48f-4250-a79d-773bf81e726b/mount
```

### Remove hanging PV storage by force

First we have to find the global resource:
```bash
kubectl -n snapshot-test get pv | rg snap
# pvc-e9105772-9717-4f3b-8538-026fff24faee              250Mi      RWO            Delete           Released    snapshot-test/mfw-volume
```

Then delete it by reusing its id:
```bash
kubectl -n snapshot-test delete persistentvolume/pvc-e9105772-9717-4f3b-8538-026fff24faee
```

This operation may take several minutes to complete since it was hanging already.
