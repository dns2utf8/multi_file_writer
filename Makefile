#SHELL := /bin/bash

publish: build_container
	docker push registry.gitlab.com/dns2utf8/multi_file_writer

build_container: update_containers
	docker build -t registry.gitlab.com/dns2utf8/multi_file_writer .
	docker image prune --force
	
update_containers:
	docker pull rust:latest
	docker pull debian:stable-slim
	
deploy_k8s_fsfreeze:
	kubectl apply -f k8s-deployment.yml
	@make get_pods
deploy_k8s_no_fsfreeze:
	kubectl apply -f k8s-deployment_no-freeze.yml
	@make get_pods
deploy_k8s_many_writes:
	kubectl apply -f k8s-deployment_RWX.yml
	@make get_pods
	
get_pods:
	kubectl get pods -n snapshot-test 

get_logs:
	kubectl -n snapshot-test logs -l app=multi-file-writer -c multi-file-writer --tail=42
get_logs_follow:
	kubectl -n snapshot-test logs -l app=multi-file-writer -c multi-file-writer --tail=42 --follow

get_storage: get_volume
get_pv: get_volume
get_volume:
	kubectl -n snapshot-test get pv | rg snapshot-test
	
get_velero_logs:
	kubectl -n velero logs deployment/velero
	
get_storage_mark:
	@echo '.metadata.annotations.appuio\.ch/backup =>' $$(kubectl -n snapshot-test get persistentvolumeclaims -o jsonpath='{.items[0].metadata.name}: {.items[0].metadata.annotations.appuio\.ch/backup}')

exec_container_shell:
	export PODNAME=$$(kubectl -n snapshot-test get pods -lapp=multi-file-writer -o jsonpath='{.items[0].metadata.name}') && \
	kubectl -n snapshot-test exec $$PODNAME -c multi-file-writer -it /bin/bash

freeze_storage:
	# info: will fail if already unfrozen
	export PODNAME=$$(kubectl -n snapshot-test get pods -lapp=multi-file-writer -o jsonpath='{.items[0].metadata.name}') && \
	kubectl -n snapshot-test exec $$PODNAME -c fsfreeze -it -- /sbin/fsfreeze --freeze /generated
unfreeze_storage:
	# info: will fail if already unfrozen
	export PODNAME=$$(kubectl -n snapshot-test get pods -lapp=multi-file-writer -o jsonpath='{.items[0].metadata.name}') && \
	kubectl -n snapshot-test exec $$PODNAME -c fsfreeze -it -- /sbin/fsfreeze --unfreeze /generated

cleanup_k8s:
	# Warning: about to delete the whole namespace snapshot-test
	@echo -n '10 ' && sleep 1
	@echo -n '9 '  && sleep 1
	@echo -n '8 '  && sleep 1
	@echo -n '7 '  && sleep 1
	@echo -n '6 '  && sleep 1
	@echo -n '5 '  && sleep 1
	@echo -n '4 '  && sleep 1
	@echo -n '3 '  && sleep 1
	@echo -n '2 '  && sleep 1
	@echo    '1 '  && sleep 1
	kubectl delete namespaces snapshot-test 

storage_mark:
	kubectl -n snapshot-test annotate pod --all --overwrite backup.velero.io/backup-volumes=mfw-volume
	@sleep 1
storage_mark_k8up_true:
	kubectl -n snapshot-test annotate persistentvolumeclaims mfw-volume --overwrite appuio.ch/backup=true
storage_mark_k8up_false:
	kubectl -n snapshot-test annotate persistentvolumeclaims mfw-volume --overwrite appuio.ch/backup=false

backup: storage_mark
	velero backup create snapshot-test-$$(date '+%Y-%m-%d-%H%M') --include-namespaces=snapshot-test --snapshot-volumes
backup_no_snapshot: storage_mark
	velero backup create snapshot-test-$$(date '+%Y-%m-%d-%H%M')-ns --include-namespaces=snapshot-test --snapshot-volumes=false
backup_hpe: storage_mark
	velero backup create snapshot-test-$$(date '+%Y-%m-%d-%H%M')-hpe --include-namespaces=snapshot-test --snapshot-volumes --volume-snapshot-locations hpe-csp
	
restart_velero:
	kubectl scale deployment/velero     --namespace velero     --replicas 0
	sleep 30
	kubectl scale deployment/velero     --namespace velero     --replicas 1

get_backups:
	velero backup get
