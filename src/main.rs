#![deny(unsafe_code)]

#[macro_use]
extern crate log;
extern crate simple_logger;

use core::ops::Range;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Barrier};
use std::thread::sleep;
use std::time::Duration;
use structopt::StructOpt;

/// A test utility for simulating a database load on disks
#[derive(StructOpt, Debug, Clone)]
//#[structopt(name = "basic")]
struct Opt {
    /// How many files should we create in parallel
    #[structopt(short, long, parse(try_from_str = parse_int::parse), default_value = "4", env = "MFW_N_FILES")]
    n_files: usize,

    /// How many lines should we create inside each file
    #[structopt(short, long, parse(try_from_str = parse_int::parse), default_value = "1_000_000", env = "MFW_ROWS")]
    rows: usize,

    /// Set how many milli seconds to wait between each synchronized append
    #[structopt(short, long, default_value = "0.1", env = "MFW_DELAY")]
    delay: f64,

    /// Output directory
    #[structopt(
        short,
        long,
        parse(from_os_str),
        default_value = "./generated",
        env = "MFW_OUTPUT_DIR"
    )]
    output_dir: PathBuf,

    /// Clean the output directory before the run
    #[structopt(short, long, env = "MFW_CLEAN_TARGET")]
    clean_target: bool,

    /// Run forever with random pauses between runs
    #[structopt(short = "f", long, env = "MFW_RUN_FOREVER")]
    run_forever: bool,

    /// When running forever, how long should the pause between runs be.
    /// This can be a range: <min..max> seconds or just <n> seconds.
    #[structopt(long, name="seconds-range", parse(try_from_str = parse_range), default_value = "60..120", env = "MFW_INTER_RUN_DELAY")]
    inter_run_delay: Range<u64>,

    /// Choose the mode to run the test in:
    /// * synced: checked progress
    /// * clock: fastest progress (accurate to +/- 1 number)
    /// * clock-catch-up: like clock allows to write multiple lines at once if needed
    #[structopt(short, long, parse(try_from_str = parse_mode), default_value = "synced", env = "MFW_MODE")]
    mode: Mode,
}

fn parse_mode(s: &str) -> Result<Mode, Box<dyn Error>>
/*where
    T: std::str::FromStr,
    T::Err: Error + 'static,
    U: std::str::FromStr,
    U::Err: Error + 'static,*/
{
    Ok(match s {
        "clock" => Mode::Clock,
        "clock-catch-up" => Mode::ClockCatchUp,
        "synced" => Mode::Synced,
        unknown => {
            use std::io::{Error, ErrorKind};
            let error = Error::new(
                ErrorKind::Other,
                format!("unknown mode {:?} [clock, clock-catch-up, synced]", unknown),
            );
            return Err(Box::new(error));
        }
    })
}

fn parse_range(s: &str) -> Result<Range<u64>, Box<dyn Error>> {
    use parse_int::parse;
    use std::io::{Error, ErrorKind};
    let mut parts = s.trim().splitn(3, ".."); //.collect::<Vec<_>>();

    let first = parts.next();
    let second = parts.next();
    let must_none = parts.next();

    if let Some(invalid) = must_none {
        let error = Error::new(
            ErrorKind::Other,
            format!("invalid range argument \"..{}\"", invalid),
        );
        return Err(Box::new(error));
    }

    Ok(match (first, second) {
        (Some(fix), None) => {
            let fix = parse(fix)?;
            Range {
                start: fix,
                end: fix + 1,
            }
        }
        (Some(start), Some(end)) => Range {
            start: parse(start)?,
            end: parse(end)?,
        },
        _ => unreachable!("this is an iterator"),
    })
}

#[derive(Debug, Clone)]
enum Mode {
    Clock,
    ClockCatchUp,
    Synced,
}
#[derive(Debug)]
enum ModeRuntime {
    Clock {
        current_max: AtomicUsize,
    },
    ClockCatchUp {
        current_max: AtomicUsize,
    },
    Synced {
        current_max: AtomicUsize,
        worker_status: Vec<AtomicUsize>,
    },
}
#[derive(Debug)]
enum MountOptions {
    RelATime,
    NoATime,
    Unknown,
}

struct SharedData {
    options: Opt,
    mode: ModeRuntime,
    //current_max: AtomicUsize,
    barrier: Barrier,
    delay_time: Duration,
}

fn main() -> std::io::Result<()> {
    simple_logger::init().expect("unable to initialize logger");

    let opt = Opt::from_args();
    info!("{:#?}", opt);

    write_config(&opt, check_is_relatime(&opt)?)?;

    if opt.run_forever {
        use rand::Rng;
        warn!("MFW_RUN_FOREVER enabled\n\n");
        let mut rng = rand::thread_rng();
        let cwd = std::env::current_dir()?;

        loop {
            run(opt.clone())?;
            std::env::set_current_dir(&cwd)?;
            let seconds = rng.gen_range(opt.inter_run_delay.start, opt.inter_run_delay.end);
            info!("Next run in {} seconds\n\n", seconds);
            sleep(Duration::from_secs(seconds));
            flush_stdout()?;
        }
    } else {
        run(opt)?;
    }

    Ok(())
}

fn run(opt: Opt) -> std::io::Result<()> {
    let pool = threadpool::Builder::new().num_threads(opt.n_files).build();

    if opt.clean_target {
        info!("removing output_dir \"{}\"", opt.output_dir.display());
        std::fs::remove_dir_all(&opt.output_dir)?;
    }

    // create target dir if necessary
    info!(
        "generating {} files in output_dir \"{}\"",
        opt.n_files,
        opt.output_dir.display()
    );
    std::fs::create_dir_all(&opt.output_dir)?;

    // cd into it
    std::env::set_current_dir(&opt.output_dir)?;

    assert!(opt.delay >= 0.0, "delay must be bigger or equal to 0.0");
    let delay_time = Duration::from_nanos((opt.delay * 1000.0).floor() as u64);

    let mode = match opt.mode {
        Mode::Clock => ModeRuntime::Clock {
            current_max: AtomicUsize::new(0),
        },
        Mode::ClockCatchUp => ModeRuntime::ClockCatchUp {
            current_max: AtomicUsize::new(0),
        },
        Mode::Synced => {
            let mut worker_status = Vec::with_capacity(opt.n_files);
            for _ in 0..opt.n_files {
                worker_status.push(AtomicUsize::new(0));
            }
            ModeRuntime::Synced {
                current_max: AtomicUsize::new(0),
                worker_status,
            }
        }
    };

    let shared_data = Arc::new(SharedData {
        delay_time,
        barrier: Barrier::new(opt.n_files + 1),
        options: opt,
        mode,
    });

    for i in 0..shared_data.options.n_files {
        let shared_data = shared_data.clone();
        pool.execute(move || {
            shared_data.barrier.wait();
            work_files(shared_data, i).expect(&format!("unable to work_files({})", i));

            info!("worker {} finished", i);
        });
    }

    shared_data.barrier.wait();
    match shared_data.options.mode {
        Mode::Clock { .. } | Mode::ClockCatchUp { .. } => give_clock(shared_data),
        Mode::Synced { .. } => give_synced(shared_data),
    }

    pool.join();
    info!("🎉 run successful 🎉");
    Ok(())
}

fn work_files(shared_data: Arc<SharedData>, file_id: usize) -> std::io::Result<()> {
    let yield_interval = shared_data.delay_time / 8;
    let mut f = File::create(format!("{}.txt", file_id))?;

    match &shared_data.mode {
        ModeRuntime::Clock { current_max } => {
            let mut global_max = current_max.load(Ordering::SeqCst);
            for i in 0..shared_data.options.rows {
                if global_max < i {
                    while {
                        global_max = current_max.load(Ordering::Acquire);
                        global_max < i
                    } {
                        sleep(yield_interval);
                    }
                }
                let buf = format!("{:?}\n", i);
                write_bytes(&mut f, &buf)?;
                f.flush()?;
            }
        }
        ModeRuntime::ClockCatchUp { current_max } => {
            let mut global_max = current_max.load(Ordering::SeqCst);
            let mut i = 0;
            while i < shared_data.options.rows {
                if global_max < i {
                    while {
                        global_max = current_max.load(Ordering::Acquire);
                        global_max < i
                    } {
                        sleep(yield_interval);
                    }
                }

                let mut buf = String::new();
                while i <= global_max {
                    buf += &format!("{:?}\n", i);
                    i += 1;
                }
                write_bytes(&mut f, &buf)?;
                f.flush()?;
            }
        }
        ModeRuntime::Synced {
            current_max,
            worker_status,
        } => {
            let worker_status = &worker_status[file_id];

            let mut global_max = current_max.load(Ordering::Acquire);
            for i in 0..shared_data.options.rows {
                if global_max < i {
                    while {
                        global_max = current_max.load(Ordering::Acquire);
                        global_max < i
                    } {
                        sleep(yield_interval);
                    }
                }
                let buf = format!("{}\n", i);
                match write_bytes(&mut f, &buf) {
                    Ok(_) => worker_status.store(i, Ordering::Release),
                    Err(e) => {
                        error!("Failed to write row {} to file {}", i, file_id);
                        worker_status.store(shared_data.options.rows, Ordering::SeqCst);
                        return Err(e);
                    }
                };
                f.flush()?;
            }
        }
    }

    Ok(())
}

// increase the current_max
fn give_clock(shared_data: Arc<SharedData>) {
    match &shared_data.mode {
        ModeRuntime::Clock { current_max } | ModeRuntime::ClockCatchUp { current_max } => {
            // make local copy for speed
            let delay = shared_data.delay_time.clone();

            for i in 0..shared_data.options.rows {
                sleep(delay);
                current_max.store(i, Ordering::SeqCst);
            }
        }
        _ => unreachable!("give_clock reached unreachable"),
    };

    info!("clock finished");
}

fn give_synced(shared_data: Arc<SharedData>) {
    match &shared_data.mode {
        ModeRuntime::Synced {
            current_max,
            worker_status,
        } => {
            let delay = shared_data.delay_time.clone();
            let short = delay / 10;

            for i in 0..shared_data.options.rows {
                current_max.store(i, Ordering::SeqCst);
                sleep(delay);

                while worker_status
                    .iter()
                    .any(|ws| ws.load(Ordering::Acquire) < i)
                {
                    sleep(short);
                }
            }
        }
        _ => unreachable!("give_synced reached unreachable"),
    }

    info!("synced control finished");
}

fn flush_stdout() -> std::io::Result<()> {
    //use std::io::Write;
    std::io::stdout().flush()?;
    std::io::stderr().flush()
}

fn check_is_relatime(opt: &Opt) -> std::io::Result<MountOptions> {
    std::fs::create_dir_all(&opt.output_dir)?;
    let output_dir = opt.output_dir.canonicalize()?;
    let output_dir = output_dir.components();
    let mounts = &std::fs::read("/proc/mounts")?;
    let mounts = String::from_utf8_lossy(mounts);
    let root = Path::new("/");

    let mut mounted_filesystem = None;
    let mut longest_match = 0;

    for line in mounts.lines() {
        // sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
        // nsfs /run/snapd/ns/spotify.mnt nsfs rw 0 0
        let mut parts = line.trim().split(" ");

        // skip source
        parts.next();

        // chop of last parts
        let mut mount_point = parts.collect::<Vec<_>>();
        let mount_options = mount_point[mount_point.len() - 3].split(',');
        mount_point.truncate(mount_point.len() - 4);

        let mount_point = mount_point.join(" ");
        let mount_point = Path::new(&mount_point);

        let mut matching_parts = 0;
        for (o, mp) in output_dir.clone().zip(mount_point.components()) {
            if o == mp {
                matching_parts += 1;
            }
        }
        if (mount_point == root && longest_match <= 1)
            || (mount_point != root && matching_parts > longest_match)
        {
            //info!("{} ({}) => {}", mount_point.display(), matching_parts, mount_options.clone().any(|a| a == "relatime"));
            longest_match = matching_parts;
            mounted_filesystem = Some((mount_point.to_owned(), mount_options));
        }
    }

    Ok(
        if let Some((mount_point, mount_options)) = mounted_filesystem {
            if mount_options.clone().any(|a| a == "relatime") {
                warn!("the output_dir is mounted on a filesystem \"{}\" with the \"relatime\" attribute! This can lead to problems with fsfreeze. Consider mounting it with \"noatime\"", mount_point.display());
                MountOptions::RelATime
            } else if mount_options.clone().any(|a| a == "noatime") {
                MountOptions::NoATime
            } else {
                warn!(
                    "neither relatime nor noatime found: {}",
                    mount_options.collect::<Vec<_>>().join(",")
                );
                MountOptions::Unknown
            }
        } else {
            warn!("unable to find mounted filesystem");
            MountOptions::Unknown
        },
    )
}

fn write_config(opt: &Opt, kind: MountOptions) -> std::io::Result<()> {
    let version = env!("CARGO_PKG_VERSION");
    let mount_kind = format!("output_dir mounted on: {:?}", kind);
    let content = format!("version: {}\n{}\n", version, mount_kind);

    info!("starting version: {}", version);
    flush_stdout()?;

    std::fs::create_dir_all(&opt.output_dir)?;
    std::fs::write(opt.output_dir.join("multi_file_writer"), content)
}

fn write_bytes(f: &mut File, buf: &str) -> std::io::Result<()> {
    let bytes = buf.as_bytes();
    let written = f.write(bytes)?;
    if written != bytes.len() {
        use std::io::{Error, ErrorKind};
        return Err(Error::new(
            ErrorKind::WriteZero,
            format!("unable to write complete row: {}/{}", written, bytes.len()),
        ));
    }
    Ok(())
}
