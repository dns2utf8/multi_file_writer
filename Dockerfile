FROM rust:latest as build

COPY ./ ./

RUN cargo build --release

RUN mkdir -p /build-out

RUN cp target/release/multi_file_writer /build-out/

# Create runtime container
# Note that we need a small init process for PID 1 that forwards signals.
# See https://github.com/Yelp/dumb-init
FROM debian:stable-slim

ENV DEBIAN_FRONTEND=noninteractive
# install watch and top since we are debugging a cluster
RUN apt-get update && apt-get -y install dumb-init procps && rm -rf /var/lib/apt/lists/*

COPY --from=build /build-out/multi_file_writer /

#RUN useradd -ms /bin/bash  multi_file_writer
#USER multi_file_writer
USER nobody

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD /multi_file_writer
